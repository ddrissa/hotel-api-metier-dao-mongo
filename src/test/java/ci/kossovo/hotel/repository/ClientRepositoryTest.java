package ci.kossovo.hotel.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit4.SpringRunner;

import ci.kossovo.hotel.documents.Adresse;
import ci.kossovo.hotel.documents.Client;
import ci.kossovo.hotel.documents.Telephone;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientRepositoryTest {
	@Autowired
	private ClientRepository clientRepository;
	
	Client traore, kouadio, diarra;
	
	@Before
	public void setUp() {
		
		clientRepository.deleteAll();
		
		Client cli1 = new Client();
		cli1.setNom("Traore");
		cli1.setPrenom("Ablo");
		cli1.setAdresse(new Adresse("tr@e2d.ci", " ", "BP 54"));
		List<Telephone>tels= new ArrayList<>();
		tels.add(new Telephone("Mobile", "07-07-07-07"));
		cli1.setNomComplet(cli1.getNom()+" "+cli1.getPrenom());
		cli1.setTelephones(tels);
		traore = clientRepository.save(cli1);
		
		Client cli2 = new Client();
		cli2.setNom("Kouadio");
		cli2.setPrenom("Ablo");
		cli2.setAdresse(new Adresse("mar@e2d.ci", " ", "BP 34"));
		cli2.setNomComplet(cli2.getNom()+" "+cli2.getPrenom());
		//cli2.setTelephones(new ArrayList<>());
		kouadio= clientRepository.save(cli2);
		
		Client cli3 = new Client();
		cli3.setNom("Diarra");
		cli3.setPrenom("Driss");
		cli3.setAdresse(new Adresse("dia@e2d.ci", " ", "BP 454"));
		cli3.setNomComplet(cli3.getNom()+" "+cli3.getPrenom());
		//cli1.setTelephones(new ArrayList<>());
		diarra = clientRepository.save(cli3);
		
		

	}
	
	@Test
	public void setsIdOnSave() {
	/*Client	cli2 = new Client();
		cli2.setNom("Kouadio");
		cli2.setPrenom("Marco");
		cli2.setAdresse(new Adresse("mar@e2d.ci", " ", "BP 34"));
		cli2.setTelephones(new ArrayList<>());*/
		Client traore = clientRepository.save(new Client("Traore", "Ablo"));
		assertThat(traore.getId()).isNotNull();
		
	}
	
	 @Test
	    public void findsByLastName() {

	        List<Client> result = clientRepository.findByNomCompletContainingIgnoreCase("Dia");

	        assertThat(result).hasSize(1).extracting("Prenom").contains("Driss");
	    }

	    @Test
	    public void findsByExample() {

	        Client probe = new Client(null, "Ablo");

	        List<Client> result = clientRepository.findAll(Example.of(probe));

	        assertThat(result).hasSize(2).extracting("nom").contains("Traore", "Kouadio");
	        
	        assertThat(result.get(0).getTelephones()).hasSize(1).extracting("libelle").contains("Mobile");
	        
	        assertThat(result.get(0).getTelephones()).hasSize(1).extracting("numero").contains("07-07-07-07");
	       
	    }

}
