package ci.kossovo.hotel.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit4.SpringRunner;

import ci.kossovo.hotel.documents.Adresse;
import ci.kossovo.hotel.documents.Employe;
import ci.kossovo.hotel.documents.Telephone;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeRepositoryTest {
	@Autowired
	private EmployeRepository employeRepository;
	
	Employe traore, kouadio, diarra;
	
	@Before
	public void setUp() {
		
		employeRepository.deleteAll();
		
		Employe emp1 = new Employe();
		emp1.setNom("Traore");
		emp1.setPrenom("Ablo");
		emp1.setAdresse(new Adresse("tr@e2d.ci", " ", "BP 54"));
		emp1.setNomComplet(emp1.getNom()+" "+emp1.getPrenom());
		List<Telephone>tels= new ArrayList<>();
		tels.add(new Telephone("Mobile", "07-07-07-07"));
		emp1.setTelephones(tels);
		traore = employeRepository.save(emp1);
		
		Employe emp2 = new Employe();
		emp2.setNom("Kouadio");
		emp2.setPrenom("Ablo");
		emp2.setAdresse(new Adresse("mar@e2d.ci", " ", "BP 34"));
		emp2.setNomComplet(emp2.getNom()+" "+emp2.getPrenom());
		/*emp2.setTelephones(new ArrayList<>());*/
		kouadio= employeRepository.save(emp2);
		
		Employe emp3 = new Employe();
		emp3.setNom("Diarra");
		emp3.setPrenom("Driss");
		emp3.setAdresse(new Adresse("dia@e2d.ci", " ", "BP 454"));
		emp3.setNomComplet(emp3.getNom()+" "+emp3.getPrenom());
		emp1.setTelephones(new ArrayList<>());
		diarra = employeRepository.save(emp3);
		
		

	}
	
	@Test
	public void setsIdOnSave() {
	
		Employe traore = employeRepository.save(new Employe("Traore", "Ablo"));
		assertThat(traore.getId()).isNotNull();
		
	}
	
	 @Test
	    public void findsByLastName() {

	        List<Employe> result = employeRepository.findByNomCompletContainingIgnoreCase("Diarr");

	        assertThat(result).hasSize(1).extracting("Prenom").contains("Driss");
	    }

	    @Test
	    public void findsByExample() {

	        Employe probe = new Employe(null, "Ablo");

	        List<Employe> result = employeRepository.findAll(Example.of(probe));

	        assertThat(result).hasSize(2).extracting("nom").contains("Traore", "Kouadio");
	        
	        assertThat(result.get(0).getTelephones()).hasSize(1).extracting("libelle").contains("Mobile");
	        
	        assertThat(result.get(0).getTelephones()).hasSize(1).extracting("numero").contains("07-07-07-07");
	       
	    }

}
