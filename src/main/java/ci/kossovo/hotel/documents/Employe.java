package ci.kossovo.hotel.documents;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document
@Getter @Setter
public class Employe extends Personne {
	private Date embauche;
	private String photo;
	private String fonction;
	private String quartier;

	public Employe() {
		super();

	}

	public Employe(String nom, String prenom, String societe, String login, String password, Boolean actived,
			String profession, Adresse adresse, List<Telephone> telephones) {
		super(nom, prenom, societe, login, password, actived, profession, adresse, telephones);

	}

	
	public Employe(String nom, String prenom) {
		super(nom, prenom);

	}

	

	@Override
	public String toString() {
		return "Employe [embauche=" + embauche + ", photo=" + photo + ", fonction=" + fonction + ", quartier="
				+ quartier + ", getEmbauche()=" + getEmbauche() + ", getPhoto()=" + getPhoto() + ", getFonction()="
				+ getFonction() + ", getQuartier()=" + getQuartier() + ", getId()=" + getId() + ", getNom()=" + getNom()
				+ ", getPrenom()=" + getPrenom() + ", getSociete()=" + getSociete() + ", getLogin()=" + getLogin()
				+ ", getPassword()=" + getPassword() + ", getActived()=" + getActived() + ", getProfession()="
				+ getProfession() + ", getAdresse()=" + getAdresse() + ", getTelephones()=" + getTelephones()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + ", getClass()=" + getClass()
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((embauche == null) ? 0 : embauche.hashCode());
		result = prime * result + ((fonction == null) ? 0 : fonction.hashCode());
		result = prime * result + ((photo == null) ? 0 : photo.hashCode());
		result = prime * result + ((quartier == null) ? 0 : quartier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employe other = (Employe) obj;
		if (embauche == null) {
			if (other.embauche != null)
				return false;
		} else if (!embauche.equals(other.embauche))
			return false;
		if (fonction == null) {
			if (other.fonction != null)
				return false;
		} else if (!fonction.equals(other.fonction))
			return false;
		if (photo == null) {
			if (other.photo != null)
				return false;
		} else if (!photo.equals(other.photo))
			return false;
		if (quartier == null) {
			if (other.quartier != null)
				return false;
		} else if (!quartier.equals(other.quartier))
			return false;
		return true;
	}

}
