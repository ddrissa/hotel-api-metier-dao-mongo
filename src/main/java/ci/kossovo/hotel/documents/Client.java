package ci.kossovo.hotel.documents;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document
@Getter @Setter
public class Client extends Personne {
	private String code;
	private String cnipassport;
	private Date naissance;
	private String lieunaissance;
	private String ville_pays;

	public Client() {
		super();

	}

	public Client(String nom, String prenom, String societe, String login, String password, Boolean actived,
			String profession, Adresse adresse, List<Telephone> telephones) {
		super(nom, prenom, societe, login, password, actived, profession, adresse, telephones);

	}

	

	public Client(String nom, String prenom, String societe, String login, String password, Boolean actived,
			String profession, Adresse adresse, List<Telephone> telephones, String code, String cni_passport,
			Date naissance, String lieunaissance, String ville_pays) {
		super(nom, prenom, societe, login, password, actived, profession, adresse, telephones);
		this.code = code;
		this.cnipassport = cni_passport;
		this.naissance = naissance;
		this.lieunaissance = lieunaissance;
		this.ville_pays = ville_pays;
	}

// Pour le test
	public Client(String nom, String prenom) {
		super(nom, prenom);

	}

	@Override
	public String toString() {
		return "Client [code=" + code + ", cniPassport=" + cnipassport + ", naissance=" + naissance + ", lieunaissance="
				+ lieunaissance + ", ville_pays=" + ville_pays + ", getCode()=" + getCode() + ", getCniPassport()="
				+ getCnipassport() + ", getNaissance()=" + getNaissance() + ", getLieunaissance()=" + getLieunaissance()
				+ ", getVille_pays()=" + getVille_pays() + ", getId()=" + getId() + ", getNom()=" + getNom()
				+ ", getPrenom()=" + getPrenom() + ", getSociete()=" + getSociete() + ", getLogin()=" + getLogin()
				+ ", getPassword()=" + getPassword() + ", getActived()=" + getActived() + ", getProfession()="
				+ getProfession() + ", getAdresse()=" + getAdresse() + ", getTelephones()=" + getTelephones()
				+ ", getNomComplet()=" + getNomComplet() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cnipassport == null) ? 0 : cnipassport.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((lieunaissance == null) ? 0 : lieunaissance.hashCode());
		result = prime * result + ((naissance == null) ? 0 : naissance.hashCode());
		result = prime * result + ((ville_pays == null) ? 0 : ville_pays.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (cnipassport == null) {
			if (other.cnipassport != null)
				return false;
		} else if (!cnipassport.equals(other.cnipassport))
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (lieunaissance == null) {
			if (other.lieunaissance != null)
				return false;
		} else if (!lieunaissance.equals(other.lieunaissance))
			return false;
		if (naissance == null) {
			if (other.naissance != null)
				return false;
		} else if (!naissance.equals(other.naissance))
			return false;
		if (ville_pays == null) {
			if (other.ville_pays != null)
				return false;
		} else if (!ville_pays.equals(other.ville_pays))
			return false;
		return true;
	}

	

	

	

}
