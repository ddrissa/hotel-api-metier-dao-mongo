package ci.kossovo.hotel.documents;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import ci.kossovo.hotel.documents.enums.EtatSejour;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Sejour {
	@Id
	private String id;
	private LocalDate sejDate;
	@DBRef
	private Client client;
	@DBRef
	private Employe employe;
	@Builder.Default
	private List<Nuite> nuites = new ArrayList();
	@Builder.Default
	private EtatSejour statut = EtatSejour.Terminer;
	private String type;
	private long montant;
	private Double remise;
	@Builder.Default
	private List<Paiement> paiements = new ArrayList<>();
	@Builder.Default
	private List<Prestation> prestations = new ArrayList<>();

	public Sejour(Client client, Employe employe, List<Nuite> nuites) {
		super();
		this.client = client;
		this.employe = employe;
		this.nuites = nuites;

	}

}
