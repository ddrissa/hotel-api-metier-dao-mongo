package ci.kossovo.hotel.documents;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class DetailPassage {

	private Date heureEntree;
	private Date heureSortie;
	private int nbreHeure;
	private Double montant;
	private Double remise;
	private Chambre chambre;

	public DetailPassage(Date heureEntree, int nbreHeure, Double montant, Chambre chambre) {
		super();
		this.heureEntree = heureEntree;
		this.nbreHeure = nbreHeure;
		this.montant = montant;
		this.chambre = chambre;
	}

}
