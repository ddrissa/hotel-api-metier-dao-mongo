package ci.kossovo.hotel.documents;

import java.time.LocalDate;

import org.springframework.data.mongodb.core.mapping.DBRef;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Nuite {
	@DBRef
	private Chambre chambre;
	private LocalDate entree;
	private LocalDate sortie;
	private long prix;
	private int jours;
	private long montant;

	/*public Nuite(Chambre chambre, LocalDate entree, LocalDate sortie) {
		super();
		this.chambre = chambre;
		this.entree = entree;
		this.sortie = sortie;
	}

	public Nuite(Chambre chambre, LocalDate entree, LocalDate sortie, long prix) {
		super();
		this.chambre = chambre;
		this.entree = entree;
		this.sortie = sortie;
		this.prix = prix;
	}
	*/
	

}
