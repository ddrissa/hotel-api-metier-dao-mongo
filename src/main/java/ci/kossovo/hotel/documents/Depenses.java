package ci.kossovo.hotel.documents;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Depenses {

	@Id
	private String id;
	@DBRef
	private Employe employe;
	private LocalDate dateDepense;
	private String type;
	private Double montant;
	private List<DetailsDepense> detailsDepenses;

	public Depenses(Employe employe, LocalDate dateDepense, String type, Double montant) {
		super();
		this.employe = employe;
		this.dateDepense = dateDepense;
		this.type = type;
		this.montant = montant;
	}

	public Depenses(Employe employe, LocalDate dateDepense, String type, Double montant,
			List<DetailsDepense> detailsDepenses) {
		super();
		this.employe = employe;
		this.dateDepense = dateDepense;
		this.type = type;
		this.montant = montant;
		this.detailsDepenses = detailsDepenses;
	}
	
	

}
