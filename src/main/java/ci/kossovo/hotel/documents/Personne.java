package ci.kossovo.hotel.documents;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public abstract class Personne {
	@Id
	private String id;
	private String nom;
	private String prenom;
	private String societe;
	private String login;
	private String password;
	private Boolean actived;
	private String profession;
	private Adresse adresse;
	private List<Telephone> telephones;
	private String nomComplet;
	
	
	public Personne() {
		super();
		this.telephones = new ArrayList<>();
		
	}
	
	public Personne(String nom, String prenom) {
		super();
		this.nom = nom;
		this.prenom = prenom;
	}

	
	public Personne(String nom, String prenom, String societe, String login, String password, Boolean actived,
			String profession, Adresse adresse, List<Telephone> telephones) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.societe = societe;
		this.login = login;
		this.password = password;
		this.actived = actived;
		this.profession = profession;
		this.adresse = adresse;
		this.telephones = telephones;
	}
	
	
}
