package ci.kossovo.hotel.documents;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Passage {
	@Id
	private String id;
	private LocalDate datePassage;
	private Employe employe;
	@Builder.Default
	private List<DetailPassage> detailPassages = new ArrayList();
	private double total;

	public Passage(LocalDate datePassage, Employe employe, List<DetailPassage> detailPassages) {
		super();
		this.datePassage = datePassage;
		this.employe = employe;
		this.detailPassages = detailPassages;
	}

}
