package ci.kossovo.hotel.documents;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import ci.kossovo.hotel.documents.enums.EtatChambre;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Chambre {

	@Id
	private String id;
	private String libelle;
	private String categorie;
	private long prix;
	@Builder.Default
	private EtatChambre etat= EtatChambre.Libre ;
	private String descriptions;

	
	
	public Chambre(String libelle, String catgorie, long prix) {
		super();
		this.libelle = libelle;
		this.categorie = catgorie;
		this.prix = prix;
		
	}



	

}
