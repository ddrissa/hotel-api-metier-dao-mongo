package ci.kossovo.hotel.documents;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Carte {
	private String liblle;
	private String numero;
	private Date dateExpiration;

	

}
