package ci.kossovo.hotel.documents.enums;

public enum EtatSejour {
	EnCours(" Sejour en cour d'execution"), Confirmer("Sejour confirmer"), Attente("Une reservation en attente de confirmation"),
	Terminer("Sejour terminé");

	private String titre = "";

	private EtatSejour(String titre) {
		this.titre = titre;
	}

	@Override
	public String toString() {
		return titre;
	}
}
