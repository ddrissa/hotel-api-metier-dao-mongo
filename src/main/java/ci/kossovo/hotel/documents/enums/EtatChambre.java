package ci.kossovo.hotel.documents.enums;

public enum EtatChambre {
	Libre, Occuper, Reserver, Indisponible

}
