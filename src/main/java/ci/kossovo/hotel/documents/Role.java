package ci.kossovo.hotel.documents;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Role {
	@Id
	private String id;
	private String nom;
	private String desc;
	public Role(String nom) {
		super();
		this.nom = nom;
	}
	public Role(String nom, String desc) {
		super();
		this.nom = nom;
		this.desc = desc;
	}
	
	

}
