package ci.kossovo.hotel.documents;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Paiement {
	private double montant;
	private String modePaiement;
	private Carte carte;

	public Paiement(double montant) {
		super();
		this.montant = montant;
	}

	

}
