package ci.kossovo.hotel.repository;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.mongodb.repository.MongoRepository;

import ci.kossovo.hotel.documents.Client;

public interface ClientRepository extends MongoRepository<Client, String> {

	List<Client> findByNomCompletContainingIgnoreCase(String mc);
	List<Client> findByCnipassportContainingIgnoreCase(String mc);
	Stream<Client> findBy();
	Client findByCnipassport(String cni);
	Client findByCode(String code);

}
