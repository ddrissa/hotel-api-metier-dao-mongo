package ci.kossovo.hotel.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.mongodb.repository.MongoRepository;

import ci.kossovo.hotel.documents.Client;
import ci.kossovo.hotel.documents.Employe;
import ci.kossovo.hotel.documents.Sejour;
import ci.kossovo.hotel.documents.enums.EtatSejour;

public interface SejourRepository extends MongoRepository<Sejour, String> {
	Stream<Sejour> findBy();

	List<Sejour> findByStatut(EtatSejour statut);

	List<Sejour> findByStatutNotContaining(EtatSejour statut);

	List<Sejour> findBySejDateBetween(LocalDate debut, LocalDate fin);

	List<Sejour> findByClient(Client c);

	List<Sejour> findByEmploye(Employe emp);

	List<Sejour> findBySejDateAfter(LocalDate d);
}
