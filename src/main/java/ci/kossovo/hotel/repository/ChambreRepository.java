package ci.kossovo.hotel.repository;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.mongodb.repository.MongoRepository;

import ci.kossovo.hotel.documents.Chambre;
import ci.kossovo.hotel.documents.enums.EtatChambre;

public interface ChambreRepository extends MongoRepository<Chambre, String>{
Stream<Chambre> findBy();
List<Chambre> findByEtatOrEtat(EtatChambre et, EtatChambre et2);
List<Chambre> findByCategorie(String cat);

}
