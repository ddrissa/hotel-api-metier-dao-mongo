package ci.kossovo.hotel.repository;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import ci.kossovo.hotel.documents.Employe;
import ci.kossovo.hotel.documents.Role;
import ci.kossovo.hotel.documents.UserRole;

public interface UserRoleRepository extends MongoRepository<UserRole, String> {
	List<UserRole> findByUser(Employe emp);
	List<UserRole> findByRole(Role role);

}
