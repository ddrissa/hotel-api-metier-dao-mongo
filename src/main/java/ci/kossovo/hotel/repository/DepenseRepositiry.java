package ci.kossovo.hotel.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.data.mongodb.repository.MongoRepository;

import ci.kossovo.hotel.documents.Depenses;
import ci.kossovo.hotel.documents.Employe;

public interface DepenseRepositiry extends MongoRepository<Depenses, String> {
	Stream<Depenses> findBy();
	List<Depenses> findByDateDepenseBetween(LocalDate debut, LocalDate fin);
	List<Depenses> findByEmploye(Employe emp);
	List<Depenses> findByType(String type);

}
