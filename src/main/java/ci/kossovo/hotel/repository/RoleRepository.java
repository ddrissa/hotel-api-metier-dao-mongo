package ci.kossovo.hotel.repository;
import org.springframework.data.mongodb.repository.MongoRepository;

import ci.kossovo.hotel.documents.Role;

public interface RoleRepository extends MongoRepository<Role, String> {
	Role findByNom(String nom);

}
