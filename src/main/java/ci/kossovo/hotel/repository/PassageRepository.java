package ci.kossovo.hotel.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import ci.kossovo.hotel.documents.Employe;
import ci.kossovo.hotel.documents.Passage;

public interface PassageRepository extends MongoRepository<Passage, String> {
	List<Passage> findByDatePassageBetween(LocalDate debut, LocalDate fin);
	List<Passage> findByEmploye(Employe emp);

}
