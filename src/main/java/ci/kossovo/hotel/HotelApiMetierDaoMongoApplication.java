package ci.kossovo.hotel;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ci.kossovo.hotel.documents.Adresse;
import ci.kossovo.hotel.documents.Chambre;
import ci.kossovo.hotel.documents.Client;
import ci.kossovo.hotel.documents.Employe;
import ci.kossovo.hotel.documents.Nuite;
import ci.kossovo.hotel.documents.Sejour;
import ci.kossovo.hotel.documents.Telephone;
import ci.kossovo.hotel.documents.enums.EtatChambre;
import ci.kossovo.hotel.documents.enums.EtatSejour;
import ci.kossovo.hotel.repository.ChambreRepository;
import ci.kossovo.hotel.repository.ClientRepository;
import ci.kossovo.hotel.repository.EmployeRepository;
import ci.kossovo.hotel.repository.SejourRepository;

@SpringBootApplication
public class HotelApiMetierDaoMongoApplication implements CommandLineRunner {
	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	private EmployeRepository employeRepository;
	@Autowired
	private SejourRepository sejourRepository;
	@Autowired
	private ChambreRepository chambreRepository;

	public static void main(String[] args) {
		SpringApplication.run(HotelApiMetierDaoMongoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Client client1 = new Client("Traore", "Abdoulaye", "", null, null, false, "Ingenieur",
				new Adresse("tr@ed.ci", "Rivera", "08 BP 55"), Arrays.asList(new Telephone("mobile", "07-08-98-44")));
		client1.setNomComplet(client1.getNom() + " " + client1.getPrenom());

		Client client2 = new Client("Kouadio", "Marcellin", null, null, null, false, "Ingenieur",
				new Adresse("marco@ed.ci", "Abobo", "21 BP 76"), Arrays.asList(new Telephone("mobile", "64-76-76-87")),
				"KM03", null, new Date(), "Sinfra", "Bouaké");

		clientRepository.deleteAll();
		clientRepository.save(client1);
		clientRepository.save(client2);

		// Employe

		Employe emp1 = new Employe("Traore", "Abdoul", "", null, null, false, "manageur",
				new Adresse("tr@ed.ci", "Rivera", "08 BP 155"), Arrays.asList(new Telephone("mobile", "07-08-98-44")));

		Employe emp2 = new Employe("Kone", "Moussa", " ", null, null, false, "Comptable", new Adresse(),
				new ArrayList<>());

		employeRepository.deleteAll();
		employeRepository.save(emp1);
		employeRepository.save(emp2);

		// Sejour
		Chambre ch1 = Chambre.builder().libelle("CH-1").categorie("GRD").prix(20000).etat(EtatChambre.Libre).build();
		// new Chambre("CH01", "GRD", 20000);
		Chambre ch2 = Chambre.builder().libelle("CH-2").categorie("GRD").prix(20000).etat(EtatChambre.Reserver).build();
		Chambre ch3 = Chambre.builder().libelle("CH-3").categorie("PT").prix(10000).etat(EtatChambre.Occuper).build();
		// new Chambre("CH02", "GRD", 20000);

		chambreRepository.deleteAll();
		chambreRepository.save(ch1);
		chambreRepository.save(ch2);
		// Chambre ch3= new Chambre("CH03", "GRD", 20000);
		
		System.out.println(".............. les chambre .........");
		chambreRepository.findByEtatOrEtat(EtatChambre.Libre, EtatChambre.Reserver).stream()
		.forEach(System.out::println);
		System.out.println(".............. Fin .........");

		Nuite nui1 = Nuite.builder().chambre(ch1).entree(LocalDate.parse("2018-05-23"))
				.sortie(LocalDate.parse("2018-05-24")).prix(ch1.getPrix()).build();

		// new Nuite(ch1, LocalDate.parse("2018-05-11"), LocalDate.parse("2018-05-14"),
		// ch1.getPrix());
		nui1.setJours(Period.between(nui1.getEntree(), nui1.getSortie()).getDays());
		nui1.setMontant(nui1.getPrix() * nui1.getJours());

		Nuite nui2 = Nuite.builder().chambre(ch2).entree(LocalDate.parse("2018-05-23"))
				.sortie(LocalDate.parse("2018-05-26")).prix(ch2.getPrix()).build();
		// new Nuite(ch2, LocalDate.parse("2018-05-11"), LocalDate.parse("2018-05-13"),
		// ch2.getPrix());

		nui2.setJours(Period.between(nui2.getEntree(), nui2.getSortie()).getDays());
		nui2.setMontant(nui2.getPrix() * nui2.getJours());

		Sejour sej = Sejour.builder().client(client1).employe(emp1).nuites(Arrays.asList(nui1, nui2))
				.sejDate(LocalDate.now()).statut(EtatSejour.EnCours).build();
		/*
		 * new Sejour(client1, emp1, Arrays.asList(nui1, nui2));
		 * sej.setSejDate(LocalDate.now());
		 */

		sejourRepository.deleteAll();
		Sejour sj = sejourRepository.save(sej);
		System.out.println(sj);
		System.out.println(".............. les nuites .........");
		sejourRepository.findBy().map(s -> s.getNuites()).forEach(System.out::println);
		System.out.println("------------- Sejour client -----------------------");
		sejourRepository.findByClient(client1).stream().forEach(System.out::println);

	}
}
